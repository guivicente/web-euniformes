import { Container, Fields, StyledLink, Title } from './styles';

export default function LoginScreen() {
  return (
    <Container>
      <Title>Login</Title>
      <Fields placeholder="E-mail" />
      <Fields placeholder="Senha" />
      <StyledLink to="/blog">Esqueci minha senha</StyledLink>
      <button type="submit">ENTRAR</button>
    </Container>
  );
}
