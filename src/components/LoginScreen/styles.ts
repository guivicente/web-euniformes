import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Container = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-top: 10.875rem;

  a {
  }

  button[type='submit'] {
    width: 33.688rem;
    padding: 0 1.5rem;
    height: 3.75rem;
    background-color: var(--blue);
    color: var(--white);
    margin-top: 6.438rem;
    border: 0;
    transition: filter 0.2s;
    cursor: pointer;

    &:hover {
      filter: brightness(0.9);
    }
  }
`;

export const Title = styled.h1`
  margin-bottom: 9.188rem;
  font-family: Moderna, sans-serif;
  font-weight: bold;
  color: var(--blue);
`;

export const Fields = styled.input`
  width: 33.688rem;
  padding: 0 1.5rem;
  height: 4rem;
  background-color: #d9d9d9;

  border: 0;

  & + input {
    margin-top: 1rem;
  }
`;

export const StyledLink = styled(Link)`
  margin-top: 1.375rem;
  margin-left: 24rem;
  &:hover {
    text-decoration: none;
  }

  color: #000000;
`;
