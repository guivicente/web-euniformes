import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 8.075rem;
  background-color: #ffffff;
  height: 100vh;

  .icon {
    width: 3.875rem;
    height: 3.875rem;
    background-color: #ffffff;
    margin-top: 5.813rem;
    color: var(--blue);
    transition: filter(0.2s);

    &:hover {
      color: var(--blue-clear);
    }

  }
`;

export const Content = styled.div`
    th {
    background-color: var(--blue);
    color: var(--white);
    border-radius: 0.45rem;
    font-size: 1.5rem;
    padding: 0.25rem 0 0.25rem 1rem;
  }
  td {
    background-color: var(--white);
    border-right: 1px solid;
    border-right-style: dotted;
    border-color: var(--gray);
    padding-bottom: 1rem;
  }
  .iconTrash {
    width: 1.8rem;
    height: 1.8rem;
    background-color: #A60D03;
    color: var(--white);
    border-radius: 0.25rem;
    cursor: pointer;

    &:hover {
      background-color: var(--red-clear);
    }
  }

  .iconEdit {
    width: 1.8rem;
    height: 1.8rem;
    background-color: var(--blue);
    color: var(--white);
    border-radius: 0.25rem;
    transition: filter(0.2s);
    cursor: pointer;

    &:hover {
      background-color: var(--blue-clear);
    }
  }
  .tableTitle {
    font-weight: bold;
    padding: 0.75rem 0 0.75rem 0;
    border-right: 1px;
    border-right-style: dotted;
    border-color: var(--gray);

  }
`;

export const TopBody = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: row;
  h1 {
    padding: 8.063rem 30rem 5rem 38rem;
    font-family: Moderna, sans-serif;
    font-weight: bold;
    color: var(--blue);
  }
  h3 {
    padding: 7.063rem 0 0 0;
    color: var(--blue);
    font-weight: normal;
  }
  a {
    padding: 7.063rem 0 0 1rem;
    text-decoration: none;
    font-size: 1.5rem;
  }
`;

export const Tables = styled.div`
  flex-direction: row;
  display: flex;
`;

export const TableLeft = styled.table`
  height: 100%;
  text-align: center;
  margin-left: 8rem;
  width: 37.5rem;

`;

export const TableRight = styled.table`
  height: 100%;
  text-align: center;
  margin-left: 8rem;
  width: 44rem;
  img {
    width: 3rem;
    height: 2rem;
  }

`;