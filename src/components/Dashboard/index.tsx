import { Link } from 'react-router-dom';
import {
  IoShirtOutline,
  IoNewspaperOutline,
  IoPersonAddOutline,
} from 'react-icons/io5';
import { IoMdTrash } from 'react-icons/io';
import { AiOutlineDashboard } from 'react-icons/ai';
import { RiEdit2Fill } from 'react-icons/ri'
import { Header, Container, TopBody, Content, TableLeft, TableRight, Tables } from './styles';
import logoFieb from '../../assets/logofieb.jpg';

export default function Dashboard() {
  return (
    <Container>
      <Header>
        <Link to="/dashboard">
          <AiOutlineDashboard className="icon" />
        </Link>
        <Link to="/blog">
          <IoShirtOutline className="icon" />
        </Link>
        <Link to="..">
          <IoNewspaperOutline className="icon" />
        </Link>
        <Link to="..">
          <IoPersonAddOutline className="icon" />
        </Link>
      </Header>
      <Content>
      <TopBody>
          <h1>Dashboard</h1>
          <h3>Bem vindo, Élcia |</h3>
          <a href=".">Sair</a>
      </TopBody>
      <Tables>
        <TableLeft>
          <tr>
            <th colSpan={4}>Cadastros</th>
          </tr>
          <tr>
            <td className="tableTitle">Nome</td>
            <td className="tableTitle">E-mail</td>
            <td colSpan={2} className="tableTitle">Ações</td>
          </tr>
          <tr>
            <td>Guilherme Afonso</td>
            <td>guilhermeafonso@hotmail.com</td>
            <td><RiEdit2Fill className="iconEdit"></RiEdit2Fill></td>
            <td><IoMdTrash className="iconTrash"></IoMdTrash></td>
          </tr>
          <tr>
            <td>Guilherme Afonso</td>
            <td>guilhermeafonso@hotmail.com</td>
            <td><RiEdit2Fill className="iconEdit"></RiEdit2Fill></td>
            <td><IoMdTrash className="iconTrash"></IoMdTrash></td>
          </tr>
        </TableLeft>
        <TableRight>
        <tr>
            <th colSpan={5}>Instituições</th>
          </tr>
          <tr>
            <td></td>
            <td className="tableTitle">Nome</td>
            <td className="tableTitle">Endereço</td>
            <td colSpan={2} className="tableTitle">Ações</td>
          </tr>
          <tr>
            <td><img src={logoFieb} alt="Logo Fieb"/></td>
            <td>FIEB Maria Theodora</td>
            <td>Av. Andrômeda, 500</td>
            <td><RiEdit2Fill className="iconEdit"></RiEdit2Fill></td>
            <td><IoMdTrash className="iconTrash"></IoMdTrash></td>
          </tr>
          <tr>
            <td><img src={logoFieb} alt="Logo Fieb"/></td>
            <td>FIEB Maria Theodora</td>
            <td>Av. Andrômeda, 500</td>
            <td><RiEdit2Fill className="iconEdit"></RiEdit2Fill></td>
            <td><IoMdTrash className="iconTrash"></IoMdTrash></td>
          </tr>
        </TableRight>
      </Tables>
      </Content>
    </Container>
  )
}
