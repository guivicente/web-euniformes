import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
`;

export const Header = styled.div`
   display: flex;
  align-items: center;
  flex-direction: column;
  width: 8.075rem;
  background-color: #ffffff;
  height: 100vh;

  .icon {
    width: 3.875rem;
    height: 3.875rem;
    background-color: #ffffff;
    margin-top: 5.813rem;
    color: var(--blue);
    transition: filter(0.2s);

    &:hover {
      color: var(--blue-clear);
    }

  }
`;

export const TopBody = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: row;
  h1 {
    padding: 8.063rem 42rem;
    font-family: Moderna, sans-serif;
    font-weight: bold;
    color: var(--blue);
  }
  h3 {
    padding: 7.063rem 0.5rem 0 0;
    color: var(--blue);
    font-weight: normal;
  }
  a {
    padding: 7.063rem 4rem 0 0;
    text-decoration: none;
    font-size: 1.5rem;
  }
`;

export const Card = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Content = styled.div`
  button {
    width: 11.825rem;
    height: 2.25rem;
    border: 0;
    cursor: pointer;
    border-radius: 1rem;
    background-color: var(--blue);
    color: var(--white);
    margin-top: 4rem;
    transition: filter(0.2s);

    &:hover {
      filter: brightness(0.9);
    }
  }
`;
export const Title = styled.p`
  padding-top: 2rem;
  color: var(--blue);
  
`;