import { Link } from 'react-router-dom';
import {
  IoShirtOutline,
  IoNewspaperOutline,
  IoPersonAddOutline,
} from 'react-icons/io5';
import { AiOutlineDashboard } from 'react-icons/ai';
import { Container, Header, TopBody, Card, Content, Title } from './styles';
import imgRectangle from '../../assets/rectangle.svg';

export default function Dashboard() {
  return (
    <Container>
      <Header>
        <Link to="/dashboard">
          <AiOutlineDashboard className="icon" />
        </Link>
        <Link to="/blog">
          <IoShirtOutline className="icon" />
        </Link>
        <Link to="..">
          <IoNewspaperOutline className="icon" />
        </Link>
        <Link to="..">
          <IoPersonAddOutline className="icon" />
        </Link>
      </Header>

      <Content>
        <TopBody>
          <h1>Blog</h1>
          <h3>Bem vindo, Élcia |</h3>
          <a href=".">Sair</a>
        </TopBody>
        <Card>
          <img src={imgRectangle} alt="Rectangle" />
          <Title>Tema</Title>
          <p>Breve descrição do conteúdo</p>
          <button>Editar</button>
        </Card>
      </Content>
    </Container>
  );
}
