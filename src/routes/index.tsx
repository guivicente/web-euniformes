import { Switch, Route } from 'react-router-dom';
import LoginScreen from '../components/LoginScreen';
import Blog from '../components/Blog';
import Dashboard from '../components/Dashboard';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={LoginScreen} />
      <Route path="/blog" component={Blog} />
      <Route path="/dashboard" component={Dashboard} />
    </Switch>
  );
}
