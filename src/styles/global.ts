import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  :root {
    --primary: #FEB6AF;
    --blue: #035AA6;
    --blue-clear: #06b4ff;
    --red-clear: #E50000;
    --white: #FFFFFF;
    --gray: #c9c9c9;
  }
    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    background-color: #F5F5F5;

  }

  html {
        @media (max-width: 1080px) {
            font-size: 93.75%; // 15px
        }
        @media (max-width: 720px) {
            font-size: 87.5%; // 14px
        }
    }
`; 